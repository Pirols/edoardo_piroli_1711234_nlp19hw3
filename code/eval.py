from utils import get_mapping, get_inverse_mapping
from nltk.corpus import wordnet as wn
import nltk

nltk.download('wordnet')


def eval_babelnet(gold_path, other_path):

    gold = open(gold_path, mode='r')
    inp = open(other_path, mode='r')

    wrong = 0
    correct = 0

    bn_to_wn = get_mapping('resources/babelnet2wordnet.tsv', sep='\t')

    for line in gold:

        splitted_line = line.rstrip().split(' ')

        ids = splitted_line[0]
        sense_keys = splitted_line[1:]

        id_inp, bn_synset = inp.readline().rstrip().split(' ')

        while ids != id_inp:
            wrong += 1
            splitted_line = line.rstrip().split(' ')
            ids = splitted_line[0]
            sense_keys = splitted_line[1:]

        other_synset = wn.of2ss(bn_to_wn[bn_synset][3:]).name()
        for sense_key in sense_keys:
            gold_synset = wn.lemma_from_key(sense_key).synset().name()

            if gold_synset == other_synset:
                correct += 1
                break

        if gold_synset != other_synset:
            wrong += 1

    gold.close()
    inp.close()

    print(correct, wrong, correct/(wrong+correct))


def eval_wndomains(gold_path, other_path):

    gold = open(gold_path, mode='r')
    inp = open(other_path, mode='r')

    wrong = 0
    correct = 0

    wn_to_bn = get_inverse_mapping('resources/babelnet2wordnet.tsv', sep='\t')
    bn_to_wndomains = get_mapping('resources/babelnet2wndomains.tsv', sep='\t')

    for line in gold:

        splitted_line = line.rstrip().split(' ')

        ids = splitted_line[0]
        sense_keys = splitted_line[1:]

        id_inp, wn_domain = inp.readline().rstrip().split(' ')

        while ids != id_inp:
            wrong += 1
            splitted_line = line.rstrip().split(' ')
            ids = splitted_line[0]
            sense_keys = splitted_line[1:]

        for sense_key in sense_keys:
            synset = wn.lemma_from_key(sense_key).synset()
            bn_synset = wn_to_bn["wn:" +
                                 str(synset.offset()).zfill(8) + synset.pos()]
            if bn_synset in bn_to_wndomains:
                gold_domain = bn_to_wndomains[bn_synset]
            else:
                gold_domain = 'factotum'

            if gold_domain == wn_domain:
                correct += 1
                break

        if gold_domain != wn_domain:
            wrong += 1

    gold.close()
    inp.close()

    print(correct, wrong, correct/(wrong+correct))


def eval_lexnames(gold_path, other_path):

    gold = open(gold_path, mode='r')
    inp = open(other_path, mode='r')

    wrong = 0
    correct = 0

    wn_to_bn = get_inverse_mapping('resources/babelnet2wordnet.tsv', sep='\t')
    bn_to_lexnames = get_mapping('resources/babelnet2lexnames.tsv', sep='\t')

    for line in gold:

        splitted_line = line.rstrip().split(' ')

        ids = splitted_line[0]
        sense_keys = splitted_line[1:]

        id_inp, lexname = inp.readline().rstrip().split(' ')

        while ids != id_inp:
            wrong += 1
            splitted_line = line.rstrip().split(' ')
            ids = splitted_line[0]
            sense_keys = splitted_line[1:]

        for sense_key in sense_keys:
            synset = wn.lemma_from_key(sense_key).synset()
            bn_synset = wn_to_bn["wn:" +
                                 str(synset.offset()).zfill(8) + synset.pos()]
            gold_lexname = bn_to_lexnames[bn_synset]

            if gold_lexname == lexname:
                correct += 1
                break

        if gold_lexname != lexname:
            wrong += 1

    gold.close()
    inp.close()

    print(correct, wrong, correct/(wrong+correct))
