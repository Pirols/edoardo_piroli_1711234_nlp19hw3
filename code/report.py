from nltk.corpus import wordnet as wn
from tensorflow._api.v1.keras.utils import plot_model

from train import build_wsd_model, setup_embeddings

import sklearn
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt

import numpy as np


def get_TSNE(embeddings, labels, width=30, height=18):

    tsne = TSNE(perplexity=30, n_components=2, init="pca")

    low_dim_embds = tsne.fit_transform(embeddings)

    plt.figure(figsize=(width, height))  # inches
    for i, label in enumerate(labels):
        x, y = low_dim_embds[i, :]
        plt.scatter(x, y)
        plt.annotate(label, xy=(x, y), xytext=(5, 2),
                     textcoords="offset points", ha="right", va="bottom")

    plt.savefig('images/tsne.png')


def get_model_plot():

    my_embeddings = 'resources/my_embeddings.txt'
    vocab_size, emb_size, emb_mat, _ = setup_embeddings(
        my_embeddings)

    input_size = 16
    hidden_size = 128
    dropout = 0.3
    rec_dropout = 0.3
    learning_rate = 0.001
    decay = 1e-5

    # output sizes
    # 75(max number of different synsets for a lemma) + 1(<PAD>) + 1(unlabelled)
    fine_output_size = 77
    n_lexnames = 47  # 45(lexnames number) + 1(<PAD>) + 1(unlabelled)
    n_wndomains = 169  # 167(wordnet domains number) + 1(<PAD>) + 1(unlabelled)

    wsd_model = build_wsd_model(input_size, fine_output_size, n_wndomains, n_lexnames, hidden_size, emb_size, emb_mat, vocab_size,
                                dropout=dropout, rec_dropout=rec_dropout, lr=learning_rate, decay=decay, print_summary=False)

    plot_model(wsd_model, to_file='images/wsd_model.png',
               show_shapes=True, show_layer_names=True)


if __name__ == "__main__":

    get_model_plot()

    embeddings_path = 'resources/my_embeddings.txt'

    # sets up for the tsne plot
    embeddings = list()
    labels = list()

    tsne_plotted_words = 100

    with open(embeddings_path, mode="r") as fd:
        # Discarding first line
        fd.readline()

        for line in fd:
            if len(labels) < tsne_plotted_words:
                if np.random.uniform(0, 1) < 0.2:
                    words = line.split()
                    labels.append(words[0])
                    embeddings.append(words[1:])

    get_TSNE(embeddings, labels)
