# Needed to unzip archives
from zipfile import ZipFile

# Needed to untar archives
import tarfile

# Needed to delete folders
from shutil import rmtree

# Needed to return np.array
import numpy as np

import nltk
from nltk.corpus import wordnet as wn


def extract_zip_files(zip_archive: str, output_path: str = None, to_extract: list = [], pwd: str = None, show_archive: bool = False) -> list:
    """
        This function unzips(totally or partially based on to_extract) zip_archive to output_path, using pwd if specified.

        extractZipFiles(zip_archive:str, output_path:str=None, to_extract:list=[], pwd:str=None, showArchive:bool=False) -> None
        takes as inputs:
            -zip_archive: The zip archive to extract
            Optionals:
                -output_path: Specifies where to extract the files, otherwise they are extracted in the same directory of the archive
                -to_extract: list of files within the archive to extract, otherwise extracts all the archive
                -pwd: specifies the password if required
                -show_archive: if set to true it will just print out the structure of the archive
        and outputs:
            -if to_extract = []: []
            -else: the list of the output paths of the files extracted
    """

    # opens the archive
    with ZipFile(zip_archive, mode='r') as myzip:

        if show_archive:
            # displays the archive tree
            myzip.printdir()
            return

        if not output_path:
            output_path = zip_archive[:zip_archive.rfind('/')]

        if not to_extract:
            # extracts it all
            myzip.extractall(path=output_path, pwd=pwd)
            return []

        out = list()

        for file in to_extract:
            myzip.extract(file, path=output_path, pwd=pwd)

            out.append(output_path + '/' + file)

    return out


def extract_tar_files(tar_archive: str, output_path: str = None, to_extract: list = [], show_archive: bool = False) -> list:
    """
        This function extracts(totally or partially based on to_extract) tar_archive to output_path.

        extractTarFiles(zip_archive:str, output_path:str=None, to_extract:list=[], showArchive:bool=False) -> None
        takes as inputs:
            -tar_archive: The tar archive to extract
            Optionals:
                -output_path: Specifies where to extract the files, otherwise they are extracted in the same directory of the archive
                -to_extract: list of files within the archive to extract, otherwise extracts all the archive
                -show_archive: if set to true it will just print out the structure of the archive
        and outputs:
            -if to_extract = [] -> []
            -else -> the list of the output paths of the files specified in to_extract
    """

    # determines which kind of compression was used -> there are others not covered here
    if tar_archive.endswith('tar.gz'):
        mode = 'r:gz'
    elif tar_archive.endswith('tar'):
        mode = 'r:'
    else:
        print('Unsupported format.')
        return

    # opens the archive
    with tarfile.open(name=tar_archive, mode=mode) as tar:

        if show_archive:
            # printing out the archive tree
            tar.list(verbose=False)
            return

        if not output_path:
            output_path = tar_archive[:tar_archive.rfind('/')]

        if not to_extract:
            # just extracts it all
            tar.extractall(path=output_path)
            return []

        out = list()

        for file in to_extract:
            tar.extract(file, path=output_path)

            out.append(output_path + '/' + file)

    return out


def rm(directory: str) -> None:
    """
        This function deletes directory even if it is full, in contrast to os.rmdir

        rm(directory:str) -> None
        takes as inputs:
            -directory: The path to the folder to be deleted
        and outputs:
            -None
    """

    rmtree(directory)


def get_mapping(mapping_file: str, sep: str = "\t") -> dict:
    """
        This function returns a dictionary which maps the first element(separated via sep) of every line of mapping_file to the second of the same line.

        get_mapping(mapping_file:str, sep:str="\t") -> dict:    
        takes as inputs:
            -mapping_file: Path to the file which contains the info for the mapping
            optionals:
                -sep: The separator from key to item in the file on every line
        and outpus:
            -a dictionary which maps every first item of a line to the second one, if divided by sep 
    """

    with open(mapping_file, mode="r") as mapping_file:

        mapping = dict()

        for line in mapping_file:

            # Splits current line
            items = line.split(sep)

            # Assigns the second item as the value corresponding to the first element
            # Willingly discarding multiple values for one key
            mapping[items[0]] = items[1][:-1] if len(items) == 2 else items[1]

    return mapping


def get_start_and_stop(l: list, sublist: list) -> (int, int):
    """
        This function returns the first 2 indices i,j such that l[i:j] = sublist or -1, -1 if sublist is not contained in l.

        get_start_and_stop(l:list, sublist:list) -> (int, int)
        takes as inputs:
            -l: a list of tokens
            -sublist: a list of tokens
        and outputs:
            -(-1, -1) if sublist is not in l
            -(start, stop) if l[start:stop] == sublist
    """

    for i in range(len(l)-len(sublist)+1):
        if l[i:i+len(sublist)] == sublist:
            return i, i+len(sublist)

    return -1, -1


def find_overlapping_indices(start: int, stop: int, voc: dict, keys: list) -> (int, int):
    """
        This function looks into the keys of voc which must be tuples of integers and checks whether there are any such tuple that forms an interval
        overlapping with [start, stop) and returns them. keys contains an ignore list, so if a tuple is both in voc and keys it won't be checked.

        find_overlapping_indices(start: int, stop: int, voc: dict, keys: list) -> (int, int)
        takes as inputs:
            -start: one index
            -stop: one index
            -voc: a vocabulary whose keys are tuples of 2 integers
            -keys: a list of tuples of 2 integers
        and outputs:
            -(i, j), where (i, j) in voc, (i, j) not in keys and such that the interval [i, j) intersects with [start, stop) or (-1, -1) if not such pair is found
    """

    for key in voc:
        if key not in keys and not (start >= key[1] or stop <= key[0]):
            return key[0], key[1]
    return -1, -1  # no overlapping indices pair has been found


def get_wordnet_pos(treebank_tag: str) -> str:
    """
        This function takes as input a treebank tag and returns the most similar wordnet tag.

        get_wordnet_pos(treebank_tag:str) -> str
        takes as inputs:
            -treebank_tag: The treebank_tag to convert
        and outputs:
            -The wordnet equivalent
    """

    # download dependencies to use wordnet
    nltk.download('wordnet', quiet=True)

    if treebank_tag.startswith("J"):
        return wn.ADJ
    elif treebank_tag.startswith("V"):
        return wn.VERB
    elif treebank_tag.startswith("R"):
        return wn.ADV
    else:
        return wn.NOUN


def save_vocabulary(voc: dict, output_path: str, n: int = 1, m: int = 1, sep: str = ',') -> None:
    """
        This function takes as input a vocabulary and saves it in the following format:
            key[0] + sep + key[1] + ... + key[n] + sep + value[0] + sep + value[1] + ... + value[m].
        The first row will be written as:
            n + sep + m

        Both key and value elements __repr__ function should be implemented.

        save_vocabulary(voc: dict, output_path: str, sep: str = ',') -> None
        takes as inputs:
            -voc: The dictionary to save
            -output_path: The path where to save the vocabulary
            -optionals:
                -sep: the separator to print between keys and values
        and outputs:
            -None
    """

    f = open(output_path, mode='w')

    f.write(str(n) + sep + str(m) + '\n')

    for key, value in voc.items():
        if n == 1:
            f.write(str(key) + sep)
        elif n != 0:
            for i in range(n):
                f.write(str(key[i]) + sep)

        if m == 1:
            f.write(str(value) + '\n')
        elif m != 0:
            for i in range(m):
                f.write(str(value[i]) + sep)
            f.write('\n')

    # Closing the file to avoid any issues
    f.close()


def recover_vocabulary(voc_path: str, sep: str = ',') -> dict:
    """
        This function takes as input a path to a file and returns the dictionary encoded into the file according to the following format:
            key[0] + sep + key[1] + ... + key[n] + sep + value[0] + sep + value[1] + ... + value[m].
        The first row must be written as:
            n + sep + m

        recover_vocabulary(voc_path: str, sep: str = ',') -> dict
        takes as inputs:
            -voc_path: The path to the file where the dictionary has been saved
            -optionals:
                -sep: the separator used in the file to separate values
        and outputs:
            -dict: a dictionary mapping every a tuple of the first n elements to a list containing the last m elements of every row.
                Actually if n == 1 the keys will just be value, likewise if m == 1, value won't be lists but values.
    """

    mapping = dict()

    f = open(voc_path, mode='r')

    n, m = f.readline().rstrip().split(sep)

    n = int(n)
    m = int(m)

    for line in f:

        splitted = line.rstrip().split(sep)

        if n == 1:
            try:
                key = float(splitted[0])
            except:
                key = splitted[0]
        else:
            temp = list()

            # This function was supposed to be as general as possible but to avoid useless complications I've tweaked it a little bit for my use-case
            # First element in my case must always be a string
            temp.append(splitted[0])

            for i in range(1, n):
                try:
                    temp.append(float(splitted[i]))
                except:
                    temp.append(splitted[i])

            # The key of a dictionary must be hashable, lists are not
            key = tuple(temp)

        if m == 1:
            try:
                value = float(splitted[-1])
            except:
                value = splitted[-1]
        else:
            value = list()
            for i in range(m):
                try:
                    value.append(float(splitted[n+i]))
                except:
                    value.append(splitted[n+1])

        mapping[key] = value

    # Closing the file to avoid any issues
    f.close()

    return mapping


def get_inverse_mapping(mapping_file: str, sep: str = "\t") -> dict:
    """
        This function returns a dictionary which maps the second element(separated via sep) of every line of mapping_file to the first element of the same line.

        get_inverse_mapping(mapping_file:str, sep:str="\t") -> dict:    
        takes as inputs:
            -mapping_file: Path to the file which contains the info for the mapping
            optionals:
                -sep: The separator from key to item in the file on every line
        and outpus:
            -a dictionary which maps every first item of a line to the second one, if divided by sep 
    """

    with open(mapping_file, mode="r") as mapping_file:

        mapping = dict()

        for line in mapping_file:

            # Splits current line
            items = line.split(sep)

            # Assigns the second item as the value corresponding to the first element
            # Willingly discarding multiple values for one key
            mapping[items[1][:-1] if len(items) == 2 else items[1]] = items[0]

    return mapping


def get_list(size, index):
    """
        This function returns an array equivalent to arr=np.zeros((size)) where arr[index]=1
    """

    res = np.zeros(size)
    res[int(index)] = 1
    return res
