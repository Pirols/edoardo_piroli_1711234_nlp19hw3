from train import build_wsd_model, setup_embeddings, classes2index
from utils import recover_vocabulary, get_wordnet_pos, get_inverse_mapping

from os import path
from string import punctuation
from lxml import etree

import numpy as np

import nltk
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag

nltk.download('wordnet', quiet=True)
nltk.download('averaged_perceptron_tagger', quiet=True)


def predict_babelnet(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <BABELSynset>" format (e.g. "d000.s000.t000 bn:01234567n").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """

    # Needed to set up the model
    my_embeddings = path.join(resources_path, 'my_embeddings.txt')

    # Setting up the embeddings
    vocab_size, emb_size, emb_mat, word_to_index = setup_embeddings(my_embeddings)

    # Hyperparameters
    input_size = 16
    hidden_size = 128
    dropout = 0.3
    recurrent_dropout = 0.3
    learning_rate = 1e-3
    decay = 1e-5

    n_synsets = 77
    n_wndomains = 169
    n_lexnames = 47

    # Build the model
    wsd_model = build_wsd_model(input_size, n_synsets, n_wndomains, n_lexnames, hidden_size,
                                emb_size, emb_mat, vocab_size, dropout=dropout, rec_dropout=recurrent_dropout,
                                lr=learning_rate, decay=decay)

    # Load the weights
    wsd_model_weights = path.join(resources_path, 'wsd_model_weights.h5')
    wsd_model.load_weights(wsd_model_weights)

    # Creates a lemmatizer for later use
    lemmatizer = WordNetLemmatizer()

    # Mapping from (lemma, index) to synset_name
    lemma_index_to_synset_path = path.join(resources_path, 'lemma_index2synset.tsv')
    to_synset = recover_vocabulary(lemma_index_to_synset_path, sep='\t')

    # Mapping from wordnet synsets to bn ones
    bn_to_wn = path.join(resources_path, 'babelnet2wordnet.tsv')
    wn_to_bn = get_inverse_mapping(bn_to_wn, sep='\t')

    out = open(output_path, mode='w')
    xml_document = etree.iterparse(input_path, tag='sentence', events=('end', ))

    for event, sentence in xml_document:

        # Sentence to feed to the model
        tokenized_sentence = list()
        # words to be labelled in the output file
        instances = list()

        for word in sentence:
            lemma = word.attrib['lemma']
            w = word.text

            # performing normal data preprocessing
            if word.tag == 'wf':
                if all(c in punctuation for c in w):
                    continue

                # writing all numbers as <NUM>
                try:
                    float(w)
                    float(lemma)
                    tokenized_sentence.append("<NUM>")
                except ValueError:
                    tokenized_sentence.append(lemma)

            else:
                # writing all numbers as <NUM>
                try:
                    float(w)
                    float(lemma)
                    tokenized_sentence.append("<NUM>")
                except ValueError:
                    tokenized_sentence.append(lemma)

                instances.append((word.attrib['id'], len(tokenized_sentence)-1))

        # getting pos_tags for later use
        sentence_pos_tags = pos_tag(tokenized_sentence)

        # actual input of the model
        input_wsd_model = list()

        for i, token in enumerate(tokenized_sentence):
            if token in word_to_index:
                input_wsd_model.append(word_to_index[token])
            else:
                lemmatized = lemmatizer.lemmatize(token, get_wordnet_pos(sentence_pos_tags[i][1]))
                if lemmatized in word_to_index:
                    input_wsd_model.append(word_to_index[lemmatized])
                else:
                    input_wsd_model.append(word_to_index['<UNK>'])

        i = 0

        while len(input_wsd_model[i*input_size:]) > input_size:
            preds = wsd_model.predict(np.array([input_wsd_model[i*input_size:(i+1)*input_size]]))

            for instance in instances:
                index = instance[1]
                if index < i*input_size or index >= (i+1)*input_size:
                    continue

                lemma = tokenized_sentence[index]
                syns = wn.synsets(lemma)
                if not syns:
                    lemma = lemmatizer.lemmatize(lemma, get_wordnet_pos(sentence_pos_tags[index][1]))
                    syns = wn.synsets(lemma)
                    if not syns:
                        continue
                
                # writing the id
                out.write(instance[0] + ' ')

                if (lemma, 0) not in to_synset:
                    # return the most frequent sense
                    wn_synset = syns[0]

                else:
                    wn_synset = wn.synset(to_synset[(lemma, np.argmax(preds[2][0][index-i*input_size][1:len(syns)+1]))])

                bn_synset = wn_to_bn['wn:' + str(wn_synset.offset()).zfill(8) + wn_synset.pos()]
                out.write(bn_synset + '\n')

            i += 1

        input_wsd_model.extend([0]*(input_size - len(input_wsd_model[i*input_size:])))
        preds = wsd_model.predict(np.array([input_wsd_model[i*input_size:]]))

        for instance in instances:
            index = instance[1]
            if index < i*input_size:
                continue

            lemma = tokenized_sentence[index]
            syns = wn.synsets(lemma)
            if not syns:
                lemma = lemmatizer.lemmatize(lemma, get_wordnet_pos(sentence_pos_tags[index][1]))
                syns = wn.synsets(lemma)
                if not syns:
                    continue
            
            # writing the id
            out.write(instance[0] + ' ')

            if (lemma, 0) not in to_synset:
                # return the most frequent sense
                wn_synset = syns[0]

            else:
                wn_synset = wn.synset(to_synset[(lemma, np.argmax(preds[2][0][index-i*input_size][1:len(syns)+1]))])

            bn_synset = wn_to_bn['wn:' + str(wn_synset.offset()).zfill(8) + wn_synset.pos()]
            out.write(bn_synset + '\n')



        # These instructions are needed because even using the iterator the parsing would form a huge tree
        # which would be, once again, impossible, for most machines to store
        # It's safe to call clear() here because no descendant will be accessed
        sentence.clear()

        # also eliminate now-empty references from the root node to <Title>
        while sentence.getprevious() is not None:
            del sentence.getparent()[0]

    del xml_document
    out.close()


def predict_wordnet_domains(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <wordnetDomain>" format (e.g. "d000.s000.t000 sport").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """

    # Needed to set up the model
    my_embeddings = path.join(resources_path, 'my_embeddings.txt')

    # Setting up the embeddings
    vocab_size, emb_size, emb_mat, word_to_index = setup_embeddings(my_embeddings)

    # Hyperparameters
    input_size = 16
    hidden_size = 128
    dropout = 0.3
    recurrent_dropout = 0.3
    learning_rate = 1e-3
    decay = 1e-5

    n_synsets = 77
    n_wndomains = 169
    n_lexnames = 47

    # Build the model
    wsd_model = build_wsd_model(input_size, n_synsets, n_wndomains, n_lexnames, hidden_size,
                                emb_size, emb_mat, vocab_size, dropout=dropout, rec_dropout=recurrent_dropout,
                                lr=learning_rate, decay=decay)

    # Load the weights
    wsd_model_weights = path.join(resources_path, 'wsd_model_weights.h5')
    wsd_model.load_weights(wsd_model_weights)

    # Creates a lemmatizer for later use
    lemmatizer = WordNetLemmatizer()

    # Mapping from wordnet synsets to bn ones
    bn_to_wndomains_path = path.join(resources_path, 'babelnet2wndomains.tsv')
    wndomain_to_index = classes2index(bn_to_wndomains_path)
    index_to_wndomain = {v: k for k, v in wndomain_to_index.items()}

    out = open(output_path, mode='w')
    xml_document = etree.iterparse(input_path, tag='sentence', events=('end', ))

    for event, sentence in xml_document:

        # Sentence to feed to the model
        tokenized_sentence = list()
        # words to be labelled in the output file
        instances = list()

        for word in sentence:
            lemma = word.attrib['lemma']
            w = word.text

            # performing normal data preprocessing
            if word.tag == 'wf':
                if all(c in punctuation for c in w):
                    continue

                # writing all numbers as <NUM>
                try:
                    float(w)
                    float(lemma)
                    tokenized_sentence.append("<NUM>")
                except ValueError:
                    tokenized_sentence.append(lemma)

            else:
                # writing all numbers as <NUM>
                try:
                    float(w)
                    float(lemma)
                    tokenized_sentence.append("<NUM>")
                except ValueError:
                    tokenized_sentence.append(lemma)

                instances.append((word.attrib['id'], len(tokenized_sentence)-1))

        # getting pos_tags for later use
        sentence_pos_tags = pos_tag(tokenized_sentence)

        # actual input of the model
        input_wsd_model = list()

        for i, token in enumerate(tokenized_sentence):
            if token in word_to_index:
                input_wsd_model.append(word_to_index[token])
            else:
                lemmatized = lemmatizer.lemmatize(token, get_wordnet_pos(sentence_pos_tags[i][1]))
                if lemmatized in word_to_index:
                    input_wsd_model.append(word_to_index[lemmatized])
                else:
                    input_wsd_model.append(word_to_index['<UNK>'])

        i = 0

        while len(input_wsd_model[i*input_size:]) > input_size:
            preds = wsd_model.predict(np.array([input_wsd_model[i*input_size:(i+1)*input_size]]))

            for instance in instances:
                index = instance[1]
                if index < i*input_size or index >= (i+1)*input_size:
                    continue

                wn_domain = index_to_wndomain[np.argmax(preds[0][0][index-i*input_size][1:-1])+1]

                out.write(instance[0] + ' ' + wn_domain + '\n')

            i += 1

        input_wsd_model.extend([0]*(input_size - len(input_wsd_model[i*input_size:])))
        preds = wsd_model.predict(np.array([input_wsd_model[i*input_size:]]))

        for instance in instances:
            index = instance[1]
            if index < i*input_size:
                continue

            wn_domain = index_to_wndomain[np.argmax(preds[0][0][index-i*input_size][1:-1])+1]
            out.write(instance[0] + ' ' + wn_domain + '\n')



        # These instructions are needed because even using the iterator the parsing would form a huge tree
        # which would be, once again, impossible, for most machines to store
        # It's safe to call clear() here because no descendant will be accessed
        sentence.clear()

        # also eliminate now-empty references from the root node to <Title>
        while sentence.getprevious() is not None:
            del sentence.getparent()[0]

    del xml_document
    out.close()


def predict_lexicographer(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <lexicographerId>" format (e.g. "d000.s000.t000 noun.animal").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """

    # Needed to set up the model
    my_embeddings = path.join(resources_path, 'my_embeddings.txt')

    # Setting up the embeddings
    vocab_size, emb_size, emb_mat, word_to_index = setup_embeddings(my_embeddings)

    # Hyperparameters
    input_size = 16
    hidden_size = 128
    dropout = 0.3
    recurrent_dropout = 0.3
    learning_rate = 1e-3
    decay = 1e-5

    n_synsets = 77
    n_wndomains = 169
    n_lexnames = 47

    # Build the model
    wsd_model = build_wsd_model(input_size, n_synsets, n_wndomains, n_lexnames, hidden_size,
                                emb_size, emb_mat, vocab_size, dropout=dropout, rec_dropout=recurrent_dropout,
                                lr=learning_rate, decay=decay)

    # Load the weights
    wsd_model_weights = path.join(resources_path, 'wsd_model_weights.h5')
    wsd_model.load_weights(wsd_model_weights)

    # Creates a lemmatizer for later use
    lemmatizer = WordNetLemmatizer()

    # Mapping from wordnet synsets to bn ones
    bn_to_lexnames_path = path.join(resources_path, 'babelnet2lexnames.tsv')
    lexnames_to_index = classes2index(bn_to_lexnames_path)
    index_to_lexnames = {v: k for k, v in lexnames_to_index.items()}

    out = open(output_path, mode='w')
    xml_document = etree.iterparse(input_path, tag='sentence', events=('end', ))

    for event, sentence in xml_document:

        # Sentence to feed to the model
        tokenized_sentence = list()
        # words to be labelled in the output file
        instances = list()

        for word in sentence:
            lemma = word.attrib['lemma']
            w = word.text

            # performing normal data preprocessing
            if word.tag == 'wf':
                if all(c in punctuation for c in w):
                    continue

                # writing all numbers as <NUM>
                try:
                    float(w)
                    float(lemma)
                    tokenized_sentence.append("<NUM>")
                except ValueError:
                    tokenized_sentence.append(lemma)

            else:
                # writing all numbers as <NUM>
                try:
                    float(w)
                    float(lemma)
                    tokenized_sentence.append("<NUM>")
                except ValueError:
                    tokenized_sentence.append(lemma)

                instances.append((word.attrib['id'], len(tokenized_sentence)-1))

        # getting pos_tags for later use
        sentence_pos_tags = pos_tag(tokenized_sentence)

        # actual input of the model
        input_wsd_model = list()

        for i, token in enumerate(tokenized_sentence):
            if token in word_to_index:
                input_wsd_model.append(word_to_index[token])
            else:
                lemmatized = lemmatizer.lemmatize(token, get_wordnet_pos(sentence_pos_tags[i][1]))
                if lemmatized in word_to_index:
                    input_wsd_model.append(word_to_index[lemmatized])
                else:
                    input_wsd_model.append(word_to_index['<UNK>'])

        i = 0

        while len(input_wsd_model[i*input_size:]) > input_size:
            preds = wsd_model.predict(np.array([input_wsd_model[i*input_size:(i+1)*input_size]]))

            for instance in instances:
                index = instance[1]
                if index < i*input_size or index >= (i+1)*input_size:
                    continue

                lexname = index_to_lexnames[np.argmax(preds[1][0][index-i*input_size][1:-1])+1]

                out.write(instance[0] + ' ' + lexname + '\n')

            i += 1

        input_wsd_model.extend([0]*(input_size - len(input_wsd_model[i*input_size:])))
        preds = wsd_model.predict(np.array([input_wsd_model[i*input_size:]]))

        for instance in instances:
            index = instance[1]
            if index < i*input_size:
                continue

            lexname = index_to_lexnames[np.argmax(preds[1][0][index-i*input_size][1:-1])+1]
            out.write(instance[0] + ' ' + lexname + '\n')



        # These instructions are needed because even using the iterator the parsing would form a huge tree
        # which would be, once again, impossible, for most machines to store
        # It's safe to call clear() here because no descendant will be accessed
        sentence.clear()

        # also eliminate now-empty references from the root node to <Title>
        while sentence.getprevious() is not None:
            del sentence.getparent()[0]

    del xml_document
    out.close()
