# useful functions used to preprocess
from utils import extract_tar_files, extract_zip_files, find_overlapping_indices, get_mapping, get_start_and_stop, rm

# used to locally untar the sew dataset
import tarfile

# used to print an ETA for parse_sew
import time

# used to unescape characters in the sew dataset
from html import unescape

# used to handle errors in parse_sew
import sys

# used to remove files
import os

# used to process huge xml files
from lxml import etree

# used for a variety of purposes
from nltk.corpus import wordnet as wn

# used to perform stopwords removal
from nltk.corpus import stopwords

# used to perform punctuation removal
from string import punctuation

# used to download some dependencies
import nltk

# used to split the datasets
from numpy.random import uniform


def split_semcor_omsti(semcor_omsti: str, semcor: str, omsti: str) -> None:
    '''
        This function is used to split the semcor+omsti dataset into 2 files: one for the semcor corpus and one for the omsti one

        split_semcor_omsti(semcor_omsti: str, semcor: str, omsti: str) -> None
        takes as inputs:
            -semcor_omsti: path to semcor_omsti.xml
            -semcor: path to semcor.xml
            -omsti: path to omsti.xml
        and outputs:
            -None
    '''

    with open(semcor_omsti, mode='r') as inp, open(semcor, mode='w') as out_s, open(omsti, mode='w') as out_o:

        # Saving the first line <?xml ...> on both the files
        first = inp.readline()
        out_s.write(first)
        out_o.write(first)

        line = inp.readline()

        while line != '<corpus lang="en" source="mun">\n':

            out_s.write(line)
            line = inp.readline()

        while line != '':

            out_o.write(line)
            line = inp.readline()


def parse_raganato(path_to_xml: str, path_to_key: str, path_to_output: str, remove_stopwords: bool = False) -> None:
    """
        This function performs the preprocessing of any xml file formatted using the raganato standard. 
        It uses iterparse so it should be possible to preprocess files no matter how big they are.

        parse_raganato(path_to_xml:str, path_to_key:str, path_to_output:str, remove_stopwords:bool=False) -> None
        takes as inputs:
            -path_to_xml: representing the path to an xml file in raganato format
            -path_to_key: representing the path to a file containing the mapping between id and keys in raganato format
            -path_to_output: representing the path to the file we want the results to be printed on
            optionals:
                -remove_stopwords: representing whether we want stopwords to be removed or not
        and outputs:
            -None
    """

    # iterates over the whole XML file
    xml_document = etree.iterparse(
        path_to_xml, tag='sentence', events=('end', ))

    # gets the mapping from ids to keys
    id_to_key = get_mapping(path_to_key, sep=' ')

    # opens output file
    output_file = open(path_to_output, mode='w')

    if remove_stopwords:
        stop_words = stopwords.words('english')

    for event, sentence in xml_document:
        # new sentence starts
        lemmatized_sentence = ''
        flag = True

        for word in sentence:

            lemma = word.attrib['lemma']
            w = word.text

            # skipping punctuation and, if requested, stopwords
            if all(c in punctuation for c in w) or (remove_stopwords and w in stop_words):
                continue

            # writing all numbers as <NUM>
            try:
                float(w)
                float(lemma)
                lemmatized_sentence += "<NUM> "
                continue
            except ValueError:
                pass

            lemmatized_sentence += lemma

            if word.tag == 'instance':
                if word.attrib['id'] not in id_to_key:
                    flag = False
                    break

                synset = wn.lemma_from_key(
                    id_to_key[word.attrib['id']]).synset()

                if synset in wn.synsets(lemma):
                    lemmatized_sentence += '###' + synset.name()
                else:
                    flag = False
                    break

            elif word.tag != 'wf':
                print('Unexpected tag: {}'.format(word.tag))
                return

            lemmatized_sentence += ' '

        if flag and '###' in lemmatized_sentence:
            output_file.write(lemmatized_sentence.rstrip() + '\n')

        # These instructions are needed because even using the iterator the parsing would form a huge tree
        # which would be, once again, impossible, for most machines to store
        # It's safe to call clear() here because no descendant will be accessed
        sentence.clear()

        # also eliminate now-empty references from the root node to <Title>
        while sentence.getprevious() is not None:
            del sentence.getparent()[0]

    del xml_document


def parse_eurosense(path_to_xml: str, path_to_output: str, path_to_mapping: str, verbose: bool = False,
                    progress: bool = False, progress_step: int = 100000, remove_stopwords: bool = False) -> None:
    """
        This function parses the High Precision version of the Eurosense dataset using the method iterparse of the module lxml.etree
        Useful reference: https://www.ibm.com/developerworks/xml/library/x-hiperfparse/
        The Eurosense High Precision dataset is huge: 22.69gb as I'm writing and it wouldn't be possible to store it entirely on most of today's computers' memory.
        This is the reason why it is a better choice to use a generator rather than to read the whole file.

        Every line of the output file is composed of the english sentence where the consistent annotated anchors have been replaced by their senses.
        One anchor is considered being 'consistent' iff:
            -The synset is in mapping, hence it has one corresponding offset in wordnet
            -The synset is coherent with the lemma according to wordnet
            -The anchor actually appears in the text

        The lines of the output file differ from the articles also because stopwords(if specified), numbers and punctuation(if tokenized) are removed, 
        and because every character is written in lowercase.

        parse_eurosense(path_to_xml:str, path_to_output:str, path_to_mapping:str, verbose:bool=False, 
                    progress:bool=False, progress_step:int=100000, remove_stopwords:bool=False) -> None
        takes as inputs:
            -path_to_xml: the path to the eurosense highprecision dataset
            -path_to_output: the path to the output file
            -path_to_mapping: tha path to a file containing the mapping between babelnet and wordnet synsets
            optionals:
                -verbose: if set to True it will print some info on the discarded anchors
                -progress: if set to True it will print a little information every progress_step sentences
                -progress_step: number of sentences to be parsed before the function prints it
                -remove_stopwords: if set to True also stopwords are removed
        and outputs:
            -None
    """

    # counters
    total_anchors = 0
    written_anchors = 0
    discarded_anchors_not_in_sentence = 0
    discarded_anchors_not_in_wordnet = 0
    discarded_anchors_not_coherent_with_wordnet = 0
    discarded_anchors_overlapping_indices = 0

    if progress:
        num_sentences = 0

    # iterates over the whole XML file
    document_iterator = etree.iterparse(
        path_to_xml, tag="sentence", events=("end", ))

    if remove_stopwords:
        # used to remove the stop_words from the articles
        stop_words = stopwords.words("english")

    # used to check whether there exists a wn offset corresponding to the provided bn synset.
    mapping = get_mapping(path_to_mapping, sep='\t')

    out = open(path_to_output, mode="w")

    for event, sentence in document_iterator:
        # new sentence
        num_sentences += 1

        if progress and num_sentences % progress_step == 0:
            print("Parsing sentence number ", num_sentences)

        for child in sentence:

            if child.tag == "text":

                if child.attrib["lang"] == "en":
                    # english sentence

                    # list of the tokens in the sentence
                    sentence_tokenized = str(child.text).split()

                    # will store the anchors
                    anchors = dict()

            elif child.tag == "annotations":

                for elem in child:
                    # new annotation

                    # I gather one annotation if and only if, it:
                    #   -is labelled with attribute lang = "en"
                    #   -the specified anchor is actually contained in the sentence
                    #   -the babelnet synset maps to at least one wordnet offset
                    #   -the lemma is coherent with the synset
                    if elem.attrib["lang"] == "en":
                        total_anchors += 1

                        # if the synset is not in wordnet I don't need to save it
                        if elem.text not in mapping:
                            discarded_anchors_not_in_wordnet += 1
                            continue

                        synset = wn.of2ss(mapping[elem.text][3:])
                        lemma = "_".join(elem.attrib["lemma"].split(" "))

                        # if the synset is not consistent with the lemma according to babelnet
                        if synset not in wn.synsets(lemma):
                            discarded_anchors_not_coherent_with_wordnet += 1
                            continue

                        start, stop = get_start_and_stop(
                            sentence_tokenized, elem.attrib["anchor"].split())
                        if start == -1:
                            # if the anchor is not found I don't need to save it
                            discarded_anchors_not_in_sentence += 1
                            continue

                        # since it might happen that multiple pairs of indices generate an interval overlapping with [start, stop)
                        # I first save all the pairs and if no one of these results being longer than (stop - start) I, only then, remove all of them
                        # removing a pair immediately might not be optimal since it's not given that the new anchor will be written
                        pairs = list()
                        old_start, old_stop = find_overlapping_indices(
                            start, stop, anchors, pairs)
                        while old_start != -1:
                            if old_stop - old_start > stop - start:

                                # found one pair of indices which corresponds to a longer anchor.
                                # the readme of high-precision version of the Eurosense dataset says to keep the longest so this can be discarded
                                discarded_anchors_overlapping_indices += 1
                                break

                            pairs.append((old_start, old_stop))
                            old_start, old_stop = find_overlapping_indices(
                                start, stop, anchors, pairs)

                        if old_start != -1:
                            # it means that the break statement in the previous while has been encountered
                            continue

                        for pair in pairs:
                            discarded_anchors_overlapping_indices += 1
                            del anchors[pair]

                        # saves the sense joining multiple-words with "_"
                        # e.g. mention = "come on" -> sense = come_on_synset
                        anchors[(start, stop)] = lemma + '###' + synset.name()

                if anchors:

                    written_anchors += len(anchors)

                    for index, word in enumerate(sentence_tokenized):
                        try:
                            float(word)
                            sentence_tokenized[index] = "<NUM>"
                        except:
                            continue

                    for key in anchors:
                        if key[1] - key[0] != 1:
                            # adds a placeholder "." in order to keep the consistency of the following indices
                            # the placeholders will be removed when removing the punctuation
                            sentence_tokenized[key[0]+1:key[1]
                                               ] = "."*(key[1]-key[0]-1)

                        # replaces the anchor with its sense
                        sentence_tokenized[key[0]] = anchors[key]

                    # outputs to the file the original sentence with lowercase characters, maybe no stop-words, no punctuation and no numbers
                    # and the consistent anchors replaced with their senses. Actually only the longest anchor if many overlapping are found
                    out.write(" ".join(token.lower() for token in sentence_tokenized if (not remove_stopwords or token.lower() not in stop_words) and
                                       any(c not in punctuation for c in token)) + "\n")

            else:
                print("Found unexpected tag: " + child.tag)

        # These instructions are needed because even using the iterator the parsing would form a huge tree
        # which would be, once again, impossible, for most machines to store
        # It's safe to call clear() here because no descendant will be accessed
        sentence.clear()

        # also eliminate now-empty references from the root node to <Title>
        while sentence.getprevious() is not None:
            del sentence.getparent()[0]

    del document_iterator

    out.close()

    # summary
    if verbose:
        print("Number of parsed anchors:", total_anchors)
        print("Number of written anchors:", written_anchors)
        print("Number of anchors discarded because not in the sentence:",
              discarded_anchors_not_in_sentence)
        print("Number of anchors discarded because the synset was not in WordNet:",
              discarded_anchors_not_in_wordnet)
        print("Number of anchors discarded because the synset was not coherent with the lemma:",
              discarded_anchors_not_coherent_with_wordnet)
        print("Number of anchors discarded because of overlapping indices:",
              discarded_anchors_overlapping_indices)


def parse_sew(path_to_tar: str, path_to_output: str, path_to_mapping: str, num_articles: int = -1,
              verbose: bool = False, progress: bool = False, progress_step: int = 100000, remove_stopwords: bool = False) -> None:
    """
        This function parses the Semantic Enriched Wikipedia conservative dataset without having to store and parse its unzipped version.
        The SEW Conservative dataset is composed of 721 folders each containing many xml files describing a wikipedia article each.

        Every line of the output file is composed of the text of one article of the dataset where the consistent annotated mentions have been replaced by their senses.
        One mention is considered being "consistent" iff:
            -The synset is in mapping, hence it has one corresponding offset in wordnet
            -The synset is coherent with the lemma according to wordnet
            -The mention actually appears in the text
        Also I will skip sentences with un-parsable characters which run into XMLSyntaxError. e.g. Emojis

        The lines of the output file differ from the articles also because numbers, stop-words(if required) and punctuation(if tokenized) are removed,
        and because every character is written in lowercase.

        parse_sew(path_to_tar: str, path_to_output: str, path_to_mapping: str, num_articles: int = -1,
            verbose: bool = False, progress: bool = False, progress_step: int = 100000, remove_stopwords: bool = False) -> None
        takes as inputs:
            -path_to_tar: the path to the sew tar archive
            -path_to_output: the path to the output file
            -path_to_mapping: the path to the mapping between bn to wn synsets
            optionals:
                -num_articles: maximum number of articles to parse, if -1, all the articles will be parsed
                -verbose: if set to True some info will be printed regarding the discarded mentions
                -progress: if set to True it will print a very basic progress update every progress_step
                -progress_step: number of articles to parse before an update is printed
                -remove_stopwords: if set to True stopwords will be removed
        and outputs:
            -None
    """

    # counters
    total_mentions = 0
    written_mentions = 0
    discarded_mentions_not_in_sentence = 0
    discarded_mentions_not_in_wordnet = 0
    discarded_mentions_not_coherent_with_wordnet = 0

    if remove_stopwords:
        # used to remove the stop_words from the articles
        stop_words = stopwords.words("english")

    # used to check whether there exists a wn offset corresponding to the provided bn synset.
    mapping = get_mapping(path_to_mapping, sep='\t')

    out = open(path_to_output, mode="w")
    archive = tarfile.open(path_to_tar)

    # Gathers one file from the archive
    xml_file = archive.next()

    # Needed to print information about the left articles
    if progress:
        epoch = time.time()
        tot = num_articles

    while xml_file != None and num_articles != 0:

        if xml_file.name[-4:] == ".xml":
            # it actually extracts the file
            archive.extract(xml_file)

            # iterates over the just extracted file
            iterator = etree.iterparse(
                xml_file.name, tag="wikiArticle", events=("end", ))

            # this try clause is needed because some articles contain particular unescaped characters(tipically emojis) which make the iterator run into XMLParseError
            # When I run into such errors I just skip the article
            try:
                for event, element in iterator:

                    if element.attrib["language"] == "EN":
                        # Only english articles

                        for child in element:

                            if child.tag == "text":
                                # List of the tokens in the unescaped sentence
                                sentence_tokenized = unescape(
                                    str(child.text)).split()

                                # Dictionary which will contain all the consistent mentions
                                mentions = dict()

                            elif child.tag == "annotations":
                                for elem in child:
                                    # one annotation tag
                                    total_mentions += 1
                                    for el in elem:
                                        # tag inside one annotation tag
                                        if el.tag == "babelNetID":
                                            if el.text in mapping:
                                                synset = wn.of2ss(
                                                    mapping[el.text][3:])
                                            else:
                                                # If the synset is not in the mapping it doesn't have a WN counterpart, hence I don't need this annotation
                                                discarded_mentions_not_in_wordnet += 1
                                                break

                                        elif el.tag == "mention":
                                            lemma = "_".join(
                                                unescape(el.text).split(" "))

                                            mention_tokenized = unescape(
                                                el.text).split(" ")
                                            start, stop = get_start_and_stop(
                                                sentence_tokenized, mention_tokenized)

                                            if start == -1:
                                                # Mention not found in the article
                                                discarded_mentions_not_in_sentence += 1
                                                break

                                            if synset not in wn.synsets(lemma):
                                                # Synset is not consistent with the lemma
                                                discarded_mentions_not_coherent_with_wordnet += 1
                                                break

                                            # creates the sense joining words with "_"
                                            # e.g. mention = "come on" -> sense = come_on_synset
                                            sense = lemma + "###" + synset.name()

                                            # saving the mention since it will be written
                                            mentions[(start, stop)] = sense

                                            written_mentions += 1

                                # If there is no mention in mentions it means that no consistently annotated mention has been found => the whole sentence will be discarded
                                if mentions:
                                    for i, word in enumerate(sentence_tokenized):
                                        try:
                                            float(word)
                                            sentence_tokenized[i] = "<NUM>"
                                        except:
                                            continue

                                    for key in mentions:
                                        if key[1] - key[0] != 1:
                                            # adds a placeholder "." in order to keep the consistency of the following indices
                                            # the placeholders will be removed when removing the punctuation
                                            sentence_tokenized[key[0]+1:key[1]
                                                               ] = "."*(key[1]-key[0]-1)
                                        # replaces the mention with its sense
                                        sentence_tokenized[key[0]
                                                           ] = mentions[key]

                                    # I remove punctuation, numbers and maybe stop words and write the resulting sentence to the output file
                                    out.write(" ".join(token.lower()
                                                       for token in sentence_tokenized
                                                       if (not remove_stopwords or token.lower() not in stop_words) and
                                                       any(c not in punctuation for c in token)) + "\n")
                # It's safe to call clear() here because no descendant will be accessed
                element.clear()

                # Also eliminate now-empty references from the root node to <Title>
                while element.getprevious() is not None:
                    del element.getparent()[0]

            except Exception as e:
                        # This exception is thought to capture the XMLSyntaxError which occurs in very few articles.
                        # The following condition is needed to avoid suppressing all the possible exceptions.
                if type(e).__name__ != "XMLSyntaxError":
                    sys.exit("Error! Code: {}, Message: {}, File: {}".format(
                        type(e).__name__, str(e), xml_file.name))

            del iterator

            # Deleting the file just parsed
            os.remove(xml_file.name)

            # Number of articles to parse
            num_articles -= 1

            if progress and num_articles % progress_step == 0 and num_articles > 0:
                # Works as a progress bar
                elapsed = time.time() - epoch
                left = int(elapsed/60 * num_articles / (tot - num_articles))
                print("Articles left to parse: {}, time elapsed: {} minutes, estimated time left: {} minutes".format(
                    num_articles, "%.2f" % round(elapsed/60, 2), left if left > 1 else "<1"))

        # next article to parse
        xml_file = archive.next()

    # closing file descriptors
    out.close()
    archive.close()

    # summary
    if verbose:
        print('Number of parsed mentions: ', total_mentions)
        print('Number of written mentions: ', written_mentions)
        print('Number of mentions discarded because not in the sentence: ',
              discarded_mentions_not_in_sentence)
        print('Number of mentions discarded because the synset was not in WordNet: ',
              discarded_mentions_not_in_wordnet)
        print('Number of mentions discarded because the synset was not coherent with the lemma: ',
              discarded_mentions_not_coherent_with_wordnet)


def parse_eval(path_to_xml: str, path_to_output: str) -> None:
    """
        This function parses the evaluation datasets.
        parse_eval(path_to_xml: str, path_to_output: str) -> None
        takes as inputs:
            -path_to_xml: the path to the file to be parsed
            -path_to_output: the path to the output file
        and outputs:
            -None
    """

    output_file = open(path_to_output, mode='w')

    # iterates over the whole XML file
    xml_document = etree.iterparse(
        path_to_xml, tag='sentence', events=('end', ))

    for event, sentence in xml_document:

        # new sentence
        lemmatized_sentence = ''

        for word in sentence:

            # new word
            lemma = unescape(word.attrib['lemma'])
            w = unescape(word.attrib['surface_form'])

            # skipping punctuation and, if requested, stopwords
            if all(c in punctuation for c in w):
                continue

            # writing all numbers as <NUM>
            try:
                float(w)
                float(lemma)
                lemmatized_sentence += '<NUM> '
                continue
            except ValueError:
                pass

            lemmatized_sentence += lemma

            try:
                synset = wn.lemma_from_key(
                    word.attrib['wn30_key']).synset()
                if synset in wn.synsets(lemma):
                    lemmatized_sentence += '###' + synset.name()
            except:
                pass

            lemmatized_sentence += ' '

        output_file.write(lemmatized_sentence.rstrip() + '\n')

        # These instructions are needed because even using the iterator the parsing would form a huge tree
        # which would be, once again, impossible, for most machines to store
        # It's safe to call clear() here because no descendant will be accessed
        sentence.clear()

        # also eliminate now-empty references from the root node to <Title>
        while sentence.getprevious() is not None:
            del sentence.getparent()[0]

    del xml_document

    output_file.close()


def join_datasets(inputs_paths: list, output_path: str) -> None:
    """
        This function takes a list of paths as input and one path as output and writes all the rows in the input files into the output one.

        join_datasets(inputs_paths: list, output_path: str) -> None
        takes as inputs:
            -input_paths: The list of the paths to be copied
            -output_path: The file where the input files will be copied on.
        and outputs:
            -None
    """

    output_file = open(output_path, mode='w')

    for path in inputs_paths:
        
        fd = open(path, mode='r')

        for line in fd:
            output_file.write(line)

        fd.close()

    output_file.close()


if __name__ == '__main__':

    # Uncomment to download the dependencies for stopwords and wordnet
    nltk.download('stopwords', quiet=True)
    nltk.download('wordnet', quiet=True)

    # Creating the folders for the datasets
    os.mkdir('resources/datasets')
    os.mkdir('resources/parsed_datasets')

    # I did not upload the files, these were the relative paths in my setup
    raganato = 'resources/datasets/WSD_Training_Corpora.zip'
    eurosense = 'resources/datasets/eurosense.v1.0.high-precision.tar.gz'
    eurosense_xml = 'resources/datasets/EuroSense/eurosense.v1.0.high-precision.xml'
    sew = 'resources/datasets/sew_conservative.tar.gz'
    output_path = 'resources/datasets'
    semcor = 'resources/datasets/semcor.xml'
    omsti = 'resources/datasets/omsti.xml'

    # parsed datasets
    parsed_semcor = 'resources/parsed_datasets/parsed_semcor.txt'
    parsed_omsti = 'resources/parsed_datasets/parsed_omsti.txt'
    parsed_eurosense = 'resources/parsed_datasets/parsed_eurosense.txt'
    parsed_sew = 'resources/parsed_datasets/parsed_sew.txt'

    dev_set = 'resources/parsed_datasets/development_dataset.txt'
    train_set = 'resources/parsed_datasets/training_dataset.txt'

    # test sets
    test_set2007 = 'resources/parsed_datasets/parsed_semeval2007.txt'
    test_set2015 = 'resources/parsed_datasets/parsed_semeval2015.txt'
    test_set2 = 'resources/parsed_datasets/parsed_senseval2.txt'
    test_set3 = 'resources/parsed_datasets/parsed_senseval3.txt'

    # Mappings from bn to wn
    bn_to_wn = 'resources/babelnet2wordnet.tsv'
    bn_to_wn_domains = 'resources/babelnet2wndomains.tsv'
    bn_to_lexnames = 'resources/babelnet2lexnames.tsv'

    # Parsing Semcor+Omsti dataset

    # These are the paths within the archive
    semcor_omsti = 'WSD_Training_Corpora/SemCor+OMSTI/semcor+omsti.data.xml'
    semcor_omsti_key = 'WSD_Training_Corpora/SemCor+OMSTI/semcor+omsti.gold.key.txt'

    # Extracting Semcor+OMSTI xml file
    # notice that semcor_omsti are updated to match the path of the extracted files
    [semcor_omsti, semcor_omsti_key] = extract_zip_files(
        raganato, output_path=output_path, to_extract=[semcor_omsti, semcor_omsti_key])

    split_semcor_omsti(semcor_omsti, semcor, omsti)

    # Removes the - now useless - joint corpus
    os.remove(semcor_omsti)

    # Actually parses the semcor and omsti corpora
    parse_raganato(semcor, semcor_omsti_key,
                   parsed_semcor, remove_stopwords=False)
    #parse_raganato(omsti, semcor_omsti_key,
    #               parsed_omsti, remove_stopwords=False)

    # Parsing Eurosense dataset

    # Untarring the eurosense archive
    #extract_tar_files(eurosense, output_path=output_path)

    #parse_eurosense(eurosense_xml, parsed_eurosense, bn_to_wn, verbose=True,
    #                progress=True, progress_step=100000, remove_stopwords=False)

    # Parsing SEW dataset

    #parse_sew(sew, parsed_sew, bn_to_wn, verbose=True, progress=True,
    #          progress_step=100000, remove_stopwords=False)

    # Merge the parsed datasets into one single file -> in the training phase I realised how long it would take to tran on all these datasets, so I've just used semcor
    #join_datasets([parsed_semcor, parsed_omsti, parse_eurosense, parsed_sew], train_set)
    join_datasets([parsed_semcor], train_set)

    # Parsing one of the sem eval dataset to be used as the development dataset
    parse_eval('datasets/raganato_semeval2013.xml', dev_set)

    # Parsing the other datasets to be used as the test datasets
    parse_eval('datasets/raganato_semeval2007.xml', test_set2007)
    parse_eval('datasets/raganato_semeval2015.xml', test_set2015)
    parse_eval('datasets/raganato_senseval2.xml', test_set2)
    parse_eval('datasets/raganato_senseval3.xml', test_set3)

    # deletes the datasets
    rm('resources/datasets')
