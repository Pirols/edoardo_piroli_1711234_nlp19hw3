# using tensorflow 1.12.0
import tensorflow as tf
from tensorflow._api.v1 import keras
from tensorflow._api.v1.keras.layers import Input, Embedding, Bidirectional, LSTM, TimeDistributed, Dense,  Flatten, Permute, Reshape, Lambda, Layer, RepeatVector, multiply

from utils import save_vocabulary, recover_vocabulary, get_mapping, get_inverse_mapping, get_list, get_wordnet_pos

import numpy as np

import nltk
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag

import multiprocessing

nltk.download('wordnet', quiet=True)
nltk.download('averaged_perceptron_tagger', quiet=True)


class NonMasking(Layer):
    def __init__(self, **kwargs):
        self.supports_masking = True
        super(NonMasking, self).__init__(**kwargs)

    def build(self, input_shape):
        input_shape = input_shape

    def compute_mask(self, input, input_mask=None):
        # do not pass the mask to the next layers
        return None

    def call(self, x, mask=None):
        return x

    def get_output_shape_for(self, input_shape):
        return input_shape


def lemma_index2synset():
    """
        This function returns a mapping from a pair (lemma, index) to a synset of that lemma
    """

    mapping = dict()

    for lemma in wn.all_lemma_names():
        i = 0

        for syn in wn.synsets(lemma):

            if all(mapping[(lemma, j)] != syn.name() for j in range(i)):
                mapping[(lemma, i)] = syn.name()
                i += 1

    return mapping


def lemma_synset2index():
    """
        This function returns a mapping from a pair (lemma, synset) to a index
    """

    mapping = dict()

    for lemma in wn.all_lemma_names():
        i = 1

        for syn in wn.synsets(lemma):

            if (lemma, syn.name()) not in mapping:
                mapping[(lemma, syn.name())] = i
                i += 1
    return mapping


def index2synset():
    """
        This function returns a mapping from a index(starting from 2) to one of the synsets in wn
    """

    mapping = dict()

    # 0 is for padding and 1 for unlabelled words
    i = 2
    for syn in wn.all_synsets():
        mapping[i] = syn.name()
        i += 1

    return mapping


def synset2index():
    """
        This function returns a mapping from all the synsets of wordnet to a unique index (starting from 2)
    """
    mapping = dict()

    # 0 is for padding and 1 for unlabelled words
    i = 2
    for syn in wn.all_synsets():
        mapping[syn.name()] = i
        i += 1

    return mapping


def classes2index(mapping_path):
    """
        This function returns the mapping from the entries following the first one to an index.
        Used to encode the lexnames and wndomains to feed the keras model.
    """

    fd = open(mapping_path, mode='r')

    classes_to_index = dict()

    # i = 0 will be used for the padding
    i = 1

    for line in fd:

        splitted_line = line.rstrip().split('\t')

        for domain in splitted_line[1:]:

            if domain not in classes_to_index:
                classes_to_index[domain] = i
                i += 1

    fd.close()

    return classes_to_index


def reduce_embeddings(path_to_embeddings, output_path):
    """
        This function generates a file containing a reduced version of path_to_embeddings embeddings.
        In particular the output file will only contain embedding for words in wn.all_lemma_names() and only one embedding for all the numbers encoded as <NUM>

        reduce_embeddings(path_to_embeddings, output_path)
        takes as inputs:
            -path_to_embeddings: The path to the embeddings to reduce
            -output_path: The path to the output file
        and outputs:
            -None
    """

    fd_in =  open(path_to_embeddings, mode='r')
    fd_out = open(output_path, mode='w')

    # I don't strip on the right since I will use the \n appended to emb_size
    _, emb_size = fd_in.readline().split(' ')

    sentences_to_save = list()

    # I'm gonna save the first number embedding as associated to <NUM> and this flag is used to do it only once
    flag_num = False

    # Only embeddings of words contained in this set are going to be saved
    lemmas = set(wn.all_lemma_names())

    for line in fd_in:
        splitted_line = line.split(' ')
        
        try:
            float(splitted_line[0])
            # It's a number if it's the first one I'm gonna save its embeddings
            # otherwise I'll go to the next line, discarding this embeddings
            if not flag_num:
                splitted_line[0] = '<NUM>'
                sentences_to_save.append(' '.join(splitted_line))
                flag_num = True
        except:
            # If it's not a number I will save this embedding only if the word is a lemma
            if splitted_line[0] in lemmas:
                sentences_to_save.append(line)

    # Writing the first line of the output file as requested by the W2V format
    fd_out.write(str(len(sentences_to_save)) + ' ' + emb_size)

    for sentence in sentences_to_save:
        fd_out.write(sentence)

    fd_in.close()
    fd_out.close()


def setup_embeddings(path_to_embeddings):
    """
        This function takes a path to an embeddings file following the W2V format and sets it up for use

        setup_embeddings(path_to_embeddings)
        takes as input:
            -path_to_embeddings: The path to the file containing the embeddings
        and outputs:
            -vocab_size: The number of different embeddings
            -emb_size: The size of the embeddings
            -emb_mat: A matrix where the i-th row contains the embeddings of the word which is mapped to i
            -word_to_index: A dictionary which maps every word to a unique index
    """

    fd = open(path_to_embeddings, mode='r')

    # First row contains only 2 integers: vocab_size and emb_size
    vocab_size, emb_size = (int(x) for x in fd.readline().rstrip().split(' '))

    # I will add both <PAD> and <UNK> to the matrix this is why I need to increase the vocab_size
    vocab_size += 2

    emb_mat = np.zeros((vocab_size, emb_size))
    word_to_index = dict()

    # Associating a [0]*emb_size vector to the padding embeddings
    word_to_index["<PAD>"] = 0
    emb_mat[0] = np.zeros(emb_size)

    # Associating a random vector to the unknown tokens embeddings
    word_to_index["<UNK>"] = 1
    emb_mat[1] = np.random.uniform(low=-1.0, high=1.0, size=(emb_size))

    # indices will start from 2 for other words, since 0 is used for the padding and one for the unknown tokens
    i = 2

    for line in fd:
        # I need to remove the \n attached to the last number of every embedding
        splitted_line = line.rstrip().split(" ")

        word_to_index[splitted_line[0]] = i
        emb_mat[i] = np.array(list(float(x) for x in splitted_line[1:]))

        i += 1

    fd.close()

    return (vocab_size, emb_size, emb_mat, word_to_index)


def build_wsd_model(input_size, fine_output_size, coarse_output_size, lexnames_output_size, 
                    hidden_size, embeddings_size, embeddings_matrix, vocab_size,
                    dropout=0.2, rec_dropout=0.2, lr=0.001, decay=1e-4,
                    print_summary=False):
    """
        This function is used to set up the model
    """

    sentence = Input(shape=(input_size, ), dtype=tf.int32, name='Sentence')
    embedded_masked_sentence = Embedding(vocab_size, embeddings_size, weights=[embeddings_matrix], trainable=False, mask_zero=True, name = 'Embedded_sentence')(sentence)
    # Non masking needed because both Permute and Reshape do not support masked inputs
    embedded_sentence = NonMasking(name='Unmasked_sentence')(embedded_masked_sentence)
    bilstm = Bidirectional(LSTM(units=hidden_size, dropout=dropout, recurrent_dropout=rec_dropout, return_sequences=True), name='BiLSTM')(embedded_sentence)
    
    # Attention Layer
    permuted_bilstm = Permute((2, 1))(bilstm)
    reshaped_bilstm = Reshape((bilstm.shape[2], input_size))(permuted_bilstm)
    dense_bilstm = Dense(input_size, activation='softmax')(reshaped_bilstm)
    mean_bilstm = Lambda(lambda x: tf.keras.backend.mean(x, axis=1), name='dim_reduction')(dense_bilstm)
    repeat_bilstm = RepeatVector(bilstm.shape[2])(mean_bilstm)
    probs = Permute((2, 1), name='Attention_vec')(repeat_bilstm)
    attention = multiply([bilstm, probs], name='Attention')

    # Output layers
    output_wn_domains = TimeDistributed(Dense(coarse_output_size, activation='softmax'), name='Output_Domains')(attention)
    output_lexnames = TimeDistributed(Dense(lexnames_output_size, activation='softmax'), name='Output_Lexnames')(attention)
    output_synsets = TimeDistributed(Dense(fine_output_size, activation='softmax'), name='Output_Synsets')(attention)

    model = tf.keras.Model(inputs=[sentence], outputs=[output_wn_domains, output_lexnames, output_synsets])

    adam = keras.optimizers.Adam(lr=lr, decay=decay, amsgrad=True)

    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

    if print_summary:
        model.summary()

    return model


def wsd_input_generator(dataset_path, word_to_index, lemma_synset_to_index_path, bn_to_wn_domains_path, 
                        bn_to_lexnames_path, bn_to_wn_path, input_size, unlabelled_wndomains=168, 
                        unlabelled_lexnames=46, unlabelled_synsets=76):
    """
        This function returns a generator which yields the data contained in the input file in a format which can then be fed to the wsd_model
    """

    fd = open(dataset_path, mode='r')

    ## Setting up some mappings
    # from (lemma, synset) to the corresponding index
    to_index = recover_vocabulary(lemma_synset_to_index_path, sep='\t')

    # from bn_synset to the corresponding wndomain
    bn_to_wndomains = get_mapping(bn_to_wn_domains, sep='\t')

    # from bn_synset to the corresponding lexname
    bn_to_lexnames = get_mapping(bn_to_lexnames_path, sep='\t')

    # from wndomains and lexnames to uniques indices
    wndomains_to_index = classes2index(bn_to_wn_domains)
    lexnames_to_index = classes2index(bn_to_lexnames_path)

    # from wn_synset to the corresponding bn_synset
    wn_to_bn = get_inverse_mapping(bn_to_wn_path, sep='\t')

    # Set up a lemmatizer used to lemmatize non-lemma words
    lemmatizer = WordNetLemmatizer()

    ## These arrays will be used many times, so it's worth it per-allocating them
    # Padding arrays
    wn_domains_padding = get_list(169, 0)
    wn_lexnames_padding = get_list(47, 0)
    wn_synsets_padding = get_list(77, 0)

    # Unlabelled arrays
    wn_domains_no_label = get_list(169, unlabelled_wndomains)
    wn_lexnames_no_label = get_list(47, unlabelled_lexnames)
    wn_synsets_no_label = get_list(77, unlabelled_synsets)

    for line in fd:

        tokens = line.rstrip().split(' ')

        # Getting only the lemmas in every sentence, excluding the synsets which are concatenated as lemma###synset
        no_synset_tokens = list(word.split('###')[0] for word in tokens)

        # Getting the pos_tags of the sentence, these will then be used to perform the lemmatization of some words
        tokens_pos_tags = pos_tag(no_synset_tokens)

        inp = list()

        out_wndomains = list()
        out_lexnames = list()
        out_synsets = list()

        for i, token in enumerate(tokens):
            
            # The index is needed to perform the lemmatization of words

            if '###' in token:

                lemma, synset_name = token.split('###')

                # if the word as-it-is, is not covered by the embeddings, it tries to lemmatize it and see if its lemma is
                # if both do not appear in the embeddings it will be mapped un <UNK>
                if lemma in word_to_index:
                    inp.append(word_to_index[lemma])
                else:
                    lemmatized = lemmatizer.lemmatize(lemma, get_wordnet_pos(tokens_pos_tags[i][1]))
                    if lemmatized in word_to_index:
                        inp.append(word_to_index[lemmatized])
                    else:
                        inp.append(word_to_index['<UNK>'])

                wn_synset = wn.synset(synset_name)

                # Getting the corresponding bn synset in order to use the mappings to wndomains and lexnames
                bn_synset = wn_to_bn["wn:" + str(wn_synset.offset()).zfill(8) + wn_synset.pos()]

                if bn_synset in bn_to_wndomains:
                    out_wndomains.append(get_list(169, wndomains_to_index[bn_to_wndomains[bn_synset]]))
                else:
                    out_wndomains.append(get_list(169, wndomains_to_index['factotum']))
                    
                out_lexnames.append(get_list(47, lexnames_to_index[bn_to_lexnames[bn_synset]]))

                # if the word as-it-is, is not contained in the mapping (word, synset) -> index, it tries to lemmatize the word
                # if both the word itself and its lemmatized version appear to not have a mapping in to_index, the whole sentence is discarded
                if (lemma, synset_name) in to_index:
                    out_synsets.append(get_list(77, to_index[(lemma, synset_name)]))
                else:
                    lemmatized = lemmatizer.lemmatize(lemma, get_wordnet_pos(tokens_pos_tags[i][1]))
                    if (lemmatized, synset_name) in to_index:
                        out_synsets.append(get_list(77, to_index[(lemmatized, synset_name)]))
                    else:
                        inp = list()

                        out_wndomains = list()
                        out_lexnames = list()
                        out_synsets = list()
                        break
            else:
                # unlabelled token
                if token in word_to_index:
                    inp.append(word_to_index[token])
                else:
                    inp.append(word_to_index['<UNK>'])

                out_wndomains.append(wn_domains_no_label)
                out_lexnames.append(wn_lexnames_no_label)
                out_synsets.append(wn_synsets_no_label)

        if len(inp) != 0:

            # splitting the sequence into substrings of len input_size and, eventually, adding padding to the last one
            i = 0

            while len(inp[i*input_size:]) > input_size:
                yield (np.array([inp[i*input_size:(i+1)*input_size]]), [np.array([out_wndomains[i*input_size:(i+1)*input_size]]), 
                                                                        np.array([out_lexnames[i*input_size:(i+1)*input_size]]), 
                                                                        np.array([out_synsets[i*input_size:(i+1)*input_size]])])
                                
                i += 1

            inp.extend([0]*(input_size - len(inp[i*input_size:])))

            while len(out_wndomains[i*input_size:]) < input_size:
                out_wndomains.append(wn_domains_padding)
                out_lexnames.append(wn_lexnames_padding)
                out_synsets.append(wn_synsets_padding)

            yield (np.array([inp[i*input_size:]]), [np.array([out_wndomains[i*input_size:]]), 
                                                    np.array([out_lexnames[i*input_size:]]), 
                                                    np.array([out_synsets[i*input_size:]])])
    fd.close()


if __name__ == "__main__":

    # Datasets
    dev_set = 'resources/parsed_datasets/development_dataset.txt'
    train_set = 'resources/parsed_datasets/training_dataset.txt'

    # Test sets
    test_set2007 = 'resources/parsed_datasets/parsed_semeval2007.txt'
    test_set2015 = 'resources/parsed_datasets/parsed_semeval2015.txt'
    test_set2 = 'resources/parsed_datasets/parsed_senseval2.txt'
    test_set3 = 'resources/parsed_datasets/parsed_senseval3.txt'

    # Mappings from bn to wn
    bn_to_wn = 'resources/babelnet2wordnet.tsv'
    bn_to_wn_domains = 'resources/babelnet2wndomains.tsv'
    bn_to_lexnames = 'resources/babelnet2lexnames.tsv'

    # Mappings for the predictions
    lemma_index_to_synset_path = 'resources/lemma_index2synset.tsv'
    lemma_synset_to_index_path = 'resources/lemma_synset2index.tsv'

    # Embeddings
    # to download the following embeddings go here: https://wikipedia2vec.github.io/wikipedia2vec/pretrained/
    #full_embeddings = 'resources/enwiki-20180420_500d.txt'
    my_embeddings = 'resources/my_embeddings.txt'

    # uncomment to test this but be aware of the fact that I did not include the full_embeddings file
    #reduce_embeddings(full_embeddings, my_embeddings)

    # Model resources
    wsd_model_weights_path = 'resources/wsd_model_weights.h5'

    # These operations are commented because I've already generated the files
    #to_synset = lemma_index2synset()
    #to_index = lemma_synset2index()

    # Saves both the mappings to a vocabulary in order to fix both of them and avoid issues with different wn versions
    #save_vocabulary(to_synset, lemma_index_to_synset_path, n=2, m=1, sep='\t')
    #save_vocabulary(to_index, lemma_synset_to_index_path, n=2, m=1, sep='\t')

    # In order to recover the vocabularies it is possible to run the following instructions
    #to_synset = recover_vocabulary(lemma_index_to_synset_path, sep='\t')
    #to_index = recover_vocabulary(lemma_synset_to_index_path, sep='\t')

    # output sizes
    fine_output_size = 77 # 75(max number of different synsets for a lemma) + 1(<PAD>) + 1(unlabelled)
    n_lexnames = 47  # 45(lexnames number) + 1(<PAD>) + 1(unlabelled)
    n_wndomains = 169  # 167(wordnet domains number) + 1(<PAD>) + 1(unlabelled)

    # Setting up the embeddings
    vocab_size, emb_size, emb_mat, word_to_index = setup_embeddings(my_embeddings)

    # Using dev_set and dev_generator as follows I've performed a gridsearch 'by-hand' over the following hyperparamters
    # input_size [16, 32, 64]
    # hidden_size [128, 256, 512, 1024]
    # dropout [0.2, 0.3, 0.4]
    # recurrent_dropout [0.2, 0.3, 0.4]
    # learning rate [0.001, 0.01]
    # decay [1e-4, 1e-5, 0]

    #dev_generator = wsd_input_generator(dev_set, word_to_index, lemma_synset_to_index, bn_to_wn_domains, bn_to_lexnames, bn_to_wn,
    #                                    input_size, unlabelled_wndomains=168, unlabelled_lexnames=46, unlabelled_synsets=76)

    # Although the results I've found are not guaranteed to be the best possible, due to the small size of the dev_set, I think they are pretty reasonable:
    # input_size = 32, hidden_size = 512, dropout = 0.4, recurrent_dropout = 0.4, learning rate = 0.001, decay = 1e-5

    # Anyway I did not use such hyperparameters to train my model since my means are not powerful enough to carry out such a task in a reasonable amount of time(within a day per epoch)
    # This is why I've chosen to train a way smaller model on a smaller dataset.
    # Hyperparameters I've actually used:

    input_size = 16
    hidden_size = 128
    dropout = 0.3
    rec_dropout = 0.3
    learning_rate= 0.001
    decay = 1e-5

    EPOCHS = 3

    # cleaning up the work space
    tf.reset_default_graph()
    keras.backend.clear_session()

    # Get the model
    wsd_model = build_wsd_model(input_size, fine_output_size, n_wndomains, n_lexnames, hidden_size, emb_size, emb_mat, vocab_size,
                                dropout=dropout, rec_dropout=rec_dropout, lr=learning_rate, decay=decay, print_summary=False)

    # find out steps_per_epoch
    train_generator = wsd_input_generator(train_set, word_to_index, lemma_synset_to_index_path, bn_to_wn_domains,
                                            bn_to_lexnames, bn_to_wn, input_size)

    train_generator_size = 0
    for _ in train_generator:
        train_generator_size += 1

    for i in range(EPOCHS):

        train_generator = wsd_input_generator(train_set, word_to_index, lemma_synset_to_index_path, bn_to_wn_domains,
                                                bn_to_lexnames, bn_to_wn, input_size)

        wsd_model.fit_generator(train_generator, epochs=1, steps_per_epoch=train_generator_size, 
                                workers=multiprocessing.cpu_count())

    wsd_model.save_weights(wsd_model_weights_path)
